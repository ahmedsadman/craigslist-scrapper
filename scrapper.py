import argparse
from craigslist import CraigslistScrapper

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-query', '--query', help='Query term', type=str)
    parser.add_argument('-max_results', '--max_results', help='Maximum number of results to scrape', type=int)
    args = parser.parse_args()
    
    scrapper = CraigslistScrapper(args.query, args.max_results)
    scrapper.scrape_results()
    scrapper.write_to_csv()