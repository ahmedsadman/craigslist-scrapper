import requests
import csv
import time
import re
from bs4 import BeautifulSoup as bs
from selenium import webdriver


class CraigslistScrapper:

    def __init__(self, search_term, max_results):
        # you can change the sub domain here
        self.base_url = 'https://miami.craigslist.org'

        self.search_url = self.base_url + '/search?query=%s&sort=rel'
        self.search_term = search_term
        self.max_results = max_results
        self.results = []

        # change executable path here
        self.driver = webdriver.Chrome(executable_path='F:\chromedriver\chromedriver.exe')
        
        self.page_source = requests.get(self.search_url % (search_term))

    def scrape_results(self):
        print('Scraping started, please wait...')
        while True:
            soup = bs(self.page_source.text, 'html.parser')
            search_results = soup.find_all(class_='result-row')

            if not search_results:
                print('No results found')
                return

            for li in search_results:
                title = li.find(class_='result-title')
                link = li.find(class_='result-image', href=True)
                selenium_source = self.driver.get(link['href'])
                contact_no, email = self.get_contact_info()
                description = self.driver.find_element_by_id('postingbody')

                self.results.append(
                    {'title': title.text, 'link': link['href'], 'contact_no': contact_no,
                     'email': email, 'description': description.text})

                print('Ads scraped: ', len(self.results))
                if len(self.results) >= self.max_results:
                    print('Scraping complete!')
                    return

            # search if next page is available
            next_button = soup.find_all('a', class_='next', href=True)
            if next_button and next_button[0]['href'] != '':
                self.page_source = requests.get(
                    self.base_url + next_button[0]['href'])
            else:
                print('Scraping complete!')
                break

    def get_contact_info(self):
        # some ads don't contain the reply button, in that case just ignore
        try:
            self.driver.execute_script(
                "document.getElementsByClassName('reply_button')[0].click()")
        except:
            pass

        try:
             # check if 'showcontact' button exists and click
            reveal_contact = self.driver.find_elements_by_class_name('showcontact')
            if reveal_contact:
                time.sleep(6)
                self.driver.execute_script(
                    "document.getElementsByClassName('showcontact')[0].click()")
        except:
            pass

        time.sleep(6)
        soup = bs(self.driver.page_source, 'html.parser')
        contact_no = soup.find(class_='reply-tel-number')
        email = soup.find('a', class_='mailapp')

        if not contact_no:
            contact_no = ''
        else:
            contact_no = contact_no.text.replace('\n', ' ').strip()
            # get rid of the phone icon
            contact_no = contact_no.split()
            contact_no = ' '.join(contact_no[1:])

        if not email:
            email = ''
        else:
            email = email.text.strip()

        re_contact_no = self.extract_contact_no()
        re_email = self.extract_email()

        if re_contact_no:
            contact_no += ', ' + re_contact_no
        if re_email:
            email += ', '+ re_email

        return contact_no, email

    def extract_contact_no(self):
        # try to extract contact no from the description
        # first remove all whitespace for better matching
        source = self.driver.find_element_by_id('postingbody')
        source = source.text
        source = source.split()
        source = ''.join(source)

        # actual match pattern
        match = re.search(r'[\+\(]?[1-9][0-9 .\-\(\)]{8,}[0-9]', source)
        if match:
            return match.group(0)
        else:
            return ''

    def extract_email(self):
        # try to extract email from the description
        source = self.driver.find_element_by_id('postingbody')
        source = source.text
        match = re.search(r'[\w\.-]+@[\w\.-]+', source)
        if match:
            return match.group(0)
        else:
            return ''

    def write_to_csv(self):
        print('Writing to CSV file')
        with open('records.csv', 'w', newline='', encoding='utf-8') as csvfile:
            fieldnames = ['title', 'description',
                          'contact_no', 'email', 'link']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            writer.writerows(self.results)
        print('Write successful')

    def __del__(self):
        self.driver.quit()
